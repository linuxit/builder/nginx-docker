FROM debian:stable
MAINTAINER admin@itk98.net

RUN apt update
RUN apt -y install gcc libssl-dev make \
    libpcre3-dev zlib1g-dev libxml2-dev \
    libxslt-dev libgd-dev libgeoip-dev \
    libperl-dev git

WORKDIR /usr/src
RUN git clone https://github.com/nginx/nginx.git

WORKDIR /usr/src/nginx
RUN ./auto/configure --with-pcre --prefix=/opt/nginx \
   --user=www-data --group=www-data --with-threads \
   --with-file-aio --with-http_ssl_module \
   --with-http_v2_module --with-http_realip_module \
   --with-http_addition_module \
   --with-http_image_filter_module \
   --with-http_geoip_module=dynamic \
   --with-http_sub_module \
   --with-http_gunzip_module \
   --with-http_gzip_static_module \
   --with-http_auth_request_module \
   --with-http_random_index_module \
   --with-http_secure_link_module \
   --with-http_degradation_module \
   --with-http_slice_module \
   --with-http_perl_module \
   --with-stream=dynamic \
   --with-stream_ssl_module \
   --with-stream_realip_module \
   --with-stream_geoip_module=dynamic \
   --with-stream_ssl_preread_module

RUN make && make install

WORKDIR /
RUN rm -rf /usr/src/nginx && apt clean all

EXPOSE 80:80/tcp
EXPOSE 443:443/tcp
STOPSIGNAL SIGTERM

ENTRYPOINT ["/opt/nginx/sbin/nginx"]
CMD ["-c", "/etc/nginx/nginx.conf"]
